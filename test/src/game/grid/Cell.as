package game.grid {
import game.grid.hex.Hex;

public class Cell extends Hex {

    public function Cell(col:int = 0, row:int = 0):void {
        super(col, row, -col - row);
    }
}
}
