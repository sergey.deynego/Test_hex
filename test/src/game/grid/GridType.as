package game.grid {
public class GridType {

    public static const HEX_VERTICAL_ODD:int = 1;
    public static const HEX_VERTICAL_EVEN:int = 2;
    public static const HEX_HORIZONTAL_ODD:int = 3;
    public static const HEX_HORIZONTAL_EVEN:int = 4;
    public static const SQUARE:int = 5;

}
}
