package game.grid {
import flash.geom.Point;

import game.grid.hex.Hex;
import game.grid.hex.Layout;
import game.grid.hex.OffsetCoord;
import game.grid.ifaces.IGrid;

public class HexagonalGrid implements IGrid {

    /**
     *
     *
     * @param columns количество столбцов
     * @param width ширина слоя
     * @param height высота слоя
     */
    public function HexagonalGrid(columns:int, width:int, height:int) {
        super();
        this._columns = columns;
        this._width = width;
        this._height = height;
        this._cell_w = int(width / columns);
        this._cell_h = int(this._cell_w * 2 / Math.sqrt(3));
        this._rows = int(height / this._cell_h);
        this._min_d = Math.min(this._cell_w, this._cell_h)
        this._layout = new Layout(Layout.POINTY, new Point(this._cell_w >> 1, this._cell_w >> 1), new Point(this._cell_w, this._cell_w))
    }

    private var _width:int;
    private var _height:int;
    private var _type:int;
    private var _cell_w:int;
    private var _cell_h:int;
    private var _min_d:int;
    private var _layout:Layout;

    private var _columns:int;

    /**
     *
     */
    public function get columns():int {
        return this._columns;
    }

    private var _rows:int;

    /**
     *
     */
    public function get rows():int {
        return this._rows;
    }

    public function get gridType():int {
        return this._type;
    }

    public function set gridType(t:int):void {
        this._type = t;
    }

    /**
     *
     */
    public function get cellWidth():int {
        return this._cell_w;
    }

    /**
     *
     */
    public function get cellHeight():int {
        return this._cell_h;
    }

    /**
     *
     * @param cell
     * @return
     */
    public function grid2pixel(cell:OffsetCoord):Point {

        return this._layout.hexToPixel(OffsetCoord.rOffsetToCube(OffsetCoord.EVEN, cell));

    }

    /**
     *
     * @param point
     * @return
     */
    public function pixel2grid(point:Point):OffsetCoord {
        return new OffsetCoord(0, 0);
    }

    /**
     *
     * @param cell
     * @return
     */
    public function getNeigbor(col:int, row:int):Array {
        var result:Array = new Array();
        var h:Hex = OffsetCoord.rOffsetToCube(OffsetCoord.EVEN, new OffsetCoord(col, row));
        var a:Array = [
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(0)),
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(1)),
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(2)),
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(3)),
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(4)),
            OffsetCoord.rOffsetFromCube(OffsetCoord.EVEN, h.neighbor(5))];
        return a; //result;
    }

    /**
     *
     */
    private function _init():void {


    }
}
}
