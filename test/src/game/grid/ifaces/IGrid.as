package game.grid.ifaces {
import flash.geom.Point;

import game.grid.hex.OffsetCoord;

public interface IGrid {

    function get gridType():int;

    function set gridType(t:int):void;

    function get cellWidth():int;

    function get cellHeight():int;

    function get columns():int;

    function get rows():int;

    /**
     * преобразовать тайл(центр) в координаты
     * @param cell
     * @return
     */
    function grid2pixel(cell:OffsetCoord):Point;

    /**
     * преобразовать координаты в тайл
     * @param point
     * @return
     */
    function pixel2grid(point:Point):OffsetCoord;

    /**
     * получить список соседей
     * @return
     */
    function getNeigbor(col:int, row:int):Array;
}
}
