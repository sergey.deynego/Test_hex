package game.grid.hex {
public class Orientation {

    public function Orientation(f0:Number, f1:Number, f2:Number, f3:Number, b0:Number,
                                b1:Number, b2:Number, b3:Number, start_angle:Number):void {
        this.f0 = f0;
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
        this.b0 = b0;
        this.b1 = b1;
        this.b2 = b2;
        this.b3 = b3;
        this.start_angle = start_angle;
    }

    public var f0:Number;
    public var f1:Number;
    public var f2:Number;
    public var f3:Number;
    public var b0:Number;
    public var b1:Number;
    public var b2:Number;
    public var b3:Number;
    public var start_angle:Number;
}
}
