package game.grid.hex {
import game.entities.errors.GameError;

public class Hex {

    public static const DIRECTIONS:Array = [
        new Hex(1, 0, -1),
        new Hex(1, -1, 0),
        new Hex(0, -1, 1),
        new Hex(-1, 0, 1),
        new Hex(-1, 1, 0),
        new Hex(0, 1, -1)
    ];

    public static const DIAGONALS:Array = [
        new Hex(2, -1, -1),
        new Hex(1, -2, 1),
        new Hex(-1, -1, 2),
        new Hex(-2, 1, 1),
        new Hex(-1, 2, -1),
        new Hex(0, 1, -1)];

    /**
     *
     * @param q
     * @param r
     * @param s
     */
    public function Hex(q:int, r:int, s:int) {
        this.q = q;
        this.r = r;
        this.s = s;
        if (q + r + s != 0) throw new GameError(GameError.ILLEGAL_HEX_PARAMS, GameError.ILLEGAL_HEX_PARAMS_CODE);
    }

    public var q:int;
    public var r:int;
    public var s:int;

    /**
     *
     * @param h
     * @return
     */
    public function add(h:Hex):Hex {
        return new Hex(this.q + h.q, this.r + h.r, this.s + h.s);
    }

    /**
     *
     * @param h
     * @return
     */
    public function sub(h:Hex):Hex {
        return new Hex(this.q - h.q, this.r - h.r, this.s - h.s);
    }

    /**
     *
     * @param k
     * @return
     */
    public function scale(k:int):Hex {
        return new Hex(q * k, r * k, s * k);
    }

    /**
     *
     * @return
     */
    public function rotateLeft():Hex {
        return new Hex(-s, -q, -r);
    }

    /**
     *
     * @return
     */
    public function rotateRight():Hex {
        return new Hex(-r, -s, -q);
    }

    /**
     *
     * @param direct
     * @return
     */
    public function direction(direct:int):Hex {
        return Hex.DIRECTIONS[direct];
    }

    /**
     *
     * @param dir
     * @return
     */
    public function neighbor(dir:int):Hex {
        return this.add(this.direction(dir));
    }

    /**
     *
     * @param dir
     * @return
     */
    public function diagonalNeigbor(dir:int):Hex {
        return this.add(Hex.DIAGONALS[dir])
    }

    /**
     *
     * @return
     */
    public function length():int {
        return int(Math.abs((this.q) + Math.abs(this.r) + Math.abs(this.s)) / 2);
    }

    /**
     *
     * @param h
     * @return
     */
    public function distance(h:Hex):int {
        return this.sub(h).length();
    }
}
}
