package game.grid.hex {
import game.entities.errors.GameError;

public class FractionHex {
    /**
     *
     * @param q
     * @param r
     * @param s
     */
    public function FractionHex(q:Number, r:Number, s:Number) {
        this.q = q;
        this.r = r;
        this.s = s;

        if (Math.round(q + r + s) != 0) throw new GameError(GameError.ILLEGAL_HEX_PARAMS,
                GameError.ILLEGAL_HEX_PARAMS_CODE);

    }

    public var q:Number;
    public var r:Number;
    public var s:Number;

    /**
     *
     * @return
     */
    public function round():Hex {
        var qi:int = (int)(Math.round(q));
        var ri:int = (int)(Math.round(r));
        var si:int = (int)(Math.round(s));
        var q_diff:Number = Math.abs(qi - q);
        var r_diff:Number = Math.abs(ri - r);
        var s_diff:Number = Math.abs(si - s);
        if (q_diff > r_diff && q_diff > s_diff) {
            qi = -ri - si;
        }
        else if (r_diff > s_diff) {
            ri = -qi - si;
        }
        else {
            si = -qi - ri;
        }
        return new Hex(qi, ri, si);
    }

    /**
     *
     * @param h
     * @param t
     * @return
     */
    public function hexLerp(h:FractionHex, t:Number):FractionHex {
        return new FractionHex(this.q * (1 - t) + h.q * t,
                this.r * (1 - t) + h.r * t,
                this.s * (1 - t) + h.s * t);
    }
}
}
