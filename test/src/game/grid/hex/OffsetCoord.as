package game.grid.hex {
public class OffsetCoord {

    public static const EVEN:int = 1;
    public static const ODD:int = -1;

    /**
     *
     * @param offset
     * @param h
     * @return
     */
    public static function qOffsetFromCube(offset:int, h:Hex):OffsetCoord {
        var c:int = h.q;
        var r:int = h.r + int((h.q + offset * (h.q & 1)) / 2);
        return new OffsetCoord(c, r);
    }

    /**
     *
     * @param offset
     * @param h
     * @return
     */
    public static function qOffsetToCube(offset:int, h:OffsetCoord):Hex {
        var q:int = h.col;
        var r:int = h.row - int((h.col + offset * (h.col & 1)) / 2);
        var s:int = -q - r;
        return new Hex(q, r, s);
    }

    /**
     *
     * @param offset
     * @param h
     * @return
     */

    public static function rOffsetFromCube(offset:int, h:Hex):OffsetCoord {
        var col:int = h.q + (int)((h.r + offset * (h.r & 1)) / 2);
        var row:int = h.r;
        return new OffsetCoord(col, row);
    }

    /**
     *
     * @param offset
     * @param h
     * @return
     */
    public static function rOffsetToCube(offset:int, h:OffsetCoord):Hex {
        var q:int = h.col - (int)((h.row + offset * (h.row & 1)) / 2);
        var r:int = h.row;
        var s:int = -q - r;
        return new Hex(q, r, s);
    }

    /**
     *
     * @param col
     * @param row
     */
    public function OffsetCoord(col:int, row:int) {
        this.row = row;
        this.col = col;
    }

    public var col:int;
    public var row:int;
}
}
