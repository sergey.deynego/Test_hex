package game.grid.hex {
import flash.geom.Point;

public class Layout {

    public static const POINTY:Orientation = new Orientation(
            Math.sqrt(3.0),
            Math.sqrt(3.0) / 2.0,
            0.0,
            3.0 / 2.0,
            Math.sqrt(3.0) / 3.0,
            -1.0 / 3.0,
            0.0,
            2.0 / 3.0,
            0.5
    );
    public static const FLAT:Orientation = new Orientation(
            3.0 / 2.0,
            0.0,
            Math.sqrt(3.0) / 2.0,
            Math.sqrt(3.0),
            2.0 / 3.0,
            0.0,
            -1.0 / 3.0,
            Math.sqrt(3.0) / 3.0,
            0.0
    );

    public function Layout(orientation:Orientation, size:Point, origin:Point) {

        this.orientation = orientation;
        this.size = size;
        this.origin = origin;
    }

    public var orientation:Orientation;
    public var size:Point;
    public var origin:Point;

    /**
     * Получить координаты центра гекса
     * @param h
     * @return
     */
    public function hexToPixel(h:Hex):Point {
        var M:Orientation = orientation;
        var x:Number = (M.f0 * h.q + M.f1 * h.r) * size.x;
        var y:Number = (M.f2 * h.q + M.f3 * h.r) * size.y;
        return new Point(x + origin.x, y + origin.y);
    }


    public function pixelToHex(p:Point):FractionHex {
        var M:Orientation = orientation;
        var pt:Point = new Point((p.x - origin.x) / size.x, (p.y - origin.y) / size.y);
        var q:Number = M.b0 * pt.x + M.b1 * pt.y;
        var r:Number = M.b2 * pt.x + M.b3 * pt.y;
        return new FractionHex(q, r, -q - r);
    }


    public function hexCornerOffset(corner:int):Point {
        var M:Orientation = orientation;
        var angle:Number = 2.0 * Math.PI * (M.start_angle - corner) / 6;
        return new Point(size.x * Math.cos(angle), size.y * Math.sin(angle));
    }


    public function polygonCorners(h:Hex):Array {
        var corners:Array = new Array();
        var center:Point = hexToPixel(h);
        for (var i:int = 0; i < 6; i++) {
            var offset:Point = hexCornerOffset(i);
            corners.add(new Point(center.x + offset.x, center.y + offset.y));
        }
        return corners;
    }

}
}
