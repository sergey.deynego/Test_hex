package game.grid.hex {
public class DoubledCoord {

    /**
     *
     * @param h
     * @return
     */
    static public function qDoubledFromCube(h:Hex):DoubledCoord {
        var col:int = h.q;
        var row:int = 2 * h.r + h.q;
        return new DoubledCoord(col, row);
    }

    /**
     *
     * @param h
     * @return
     */

    static public function rDoubledFromCube(h:Hex):DoubledCoord {
        var col:int = 2 * h.q + h.r;
        var row:int = h.r;
        return new DoubledCoord(col, row);
    }

    /**
     *
     * @param col
     * @param row
     */
    public function DoubledCoord(col:int, row:int) {
        this.col = col;
        this.row = row;
    }

    public var col:int;
    public var row:int;

    /**
     *
     * @return
     */

    public function qDoubledToCube():Hex {
        var q:int = col;
        var r:int = (int)((row - col) / 2);
        var s:int = -q - r;
        return new Hex(q, r, s);
    }

    /**
     *
     * @return
     */

    public function rDoubledToCube():Hex {
        var q:int = (int)((col - row) / 2);
        var r:int = row;
        var s:int = -q - r;
        return new Hex(q, r, s);
    }
}
}
