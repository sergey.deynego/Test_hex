package game.entities.chips {
public class RedChip extends AbstractChip {
    public function RedChip(radius:uint) {
        super(ChipType.RED, radius, ChipType.RED_TYPE)
    }
}
}
