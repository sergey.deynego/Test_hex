package game.entities.chips {
import flash.display.Sprite;
import flash.events.Event;

import game.entities.ifaces.IChip;

internal class AbstractChip extends Sprite implements IChip {


    public function AbstractChip(color:uint, radius:uint, type:uint) {
        super();
        addEventListener(Event.ADDED_TO_STAGE, this.$init)
        $_color = color;
        $_radius = radius;
        $_type = type;
        this._visited = false;
    }

    protected var $_type:uint;
    protected var $_color:uint;
    protected var $_radius:uint;
    protected var $_column:int;
    protected var $_row:int;
    private var _visited:Boolean;

    final override public function get name():String {
        super.name = String(this.$_column + "_" + this.$_row);
        return super.name;
    }

    final override public function set name(s:String):void {
        super.name = s
    }

    public function get type():uint {
        return this.$_type;
    }

    public function get column():int {
        return this.$_column;
    }

    public function set column(c:int):void {
        this.$_column = c;
    }

    public function get row():int {
        return this.$_row;
    }

    public function set row(r:int):void {
        this.$_row = r;
    }

    public function get isVisited():Boolean {
        return this._visited;
    }

    public function visited():void {
        this._visited = true;
    }

    public function notVisited():void {
        this._visited = false;
    }

    protected function $imagine(color:uint, radius:uint):void {
        this.graphics.beginFill(color);
        this.graphics.drawCircle(0, 0, radius);
        this.graphics.endFill()
    }

    protected function $init(e:Event):void {
        e.stopImmediatePropagation()
        this.removeEventListener(Event.ADDED_TO_STAGE, $init)
        this.$imagine(this.$_color, this.$_radius)
    }
}
}
