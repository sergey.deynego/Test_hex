package game.entities.chips {
public class GrayChip extends AbstractChip {
    public function GrayChip(radius:uint) {
        super(ChipType.GRAY, radius, ChipType.GRAY_TYPE);
    }
}
}
