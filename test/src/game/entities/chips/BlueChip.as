package game.entities.chips {
public class BlueChip extends AbstractChip {

    public function BlueChip(r:uint) {
        super(ChipType.BLUE, r, ChipType.BLUE_TYPE)
    }
}
}
