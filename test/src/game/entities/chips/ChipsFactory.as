package game.entities.chips {
import game.entities.errors.GameError;
import game.entities.ifaces.IChip;

public class ChipsFactory {

    public function ChipsFactory(chip_radius:uint) {
        this._radius = chip_radius;
    }

    private var _radius:uint;

    /**
     * Получить фишку заданного типа. Если типа не существует генерируется ошибка
     * @param id
     * @return
     */
    public function getChip(id:uint):IChip {
        switch (id) {
            case ChipType.GREEN_TYPE:
                return new GreenChip(this._radius);
                break;
            case ChipType.RED_TYPE:
                return new RedChip(this._radius);
                break;
            case ChipType.BLUE_TYPE:
                return new BlueChip(this._radius);
                break;
            case ChipType.YELLOW_TYPE:
                return new YellowChip(this._radius);
                break;
            case ChipType.ORANGE_TYPE:
                return new OrangeChip(this._radius);
                break;
            case ChipType.GRAY_TYPE:
                return new GrayChip(this._radius);
                break;
            default:
                throw new GameError(GameError.UNKNOWN_CHIP_ID, GameError.UNKNOWN_CHIP_ID_CODE)
        }
        return null;
    }

    /**
     *
     * @param list - список возможных типов фишек из которого будет выбран случайный вариант. Если не задан, то
     * из всех возможных вариантов
     */
    public function getRandomChips(list:Array = null):IChip {
        var min:int = ChipType.MIN_CHIP_TYPE;
        var max:int = ChipType.MAX_CHIP_TYPE;
        if (list && list.length) {
            min = 0;
            max = list.length - 1;
        }
        var id:int = int((Math.floor(Math.random() * (max - min + 1)) + min));
        if (list && list.length) {
            id = list[id];
        }
        return getChip(id);
    }
}
}
