package game.entities.chips {
public class OrangeChip extends AbstractChip {
    public function OrangeChip(radius:uint) {
        super(ChipType.ORANGE, radius, ChipType.ORANGE_TYPE);
    }
}
}
