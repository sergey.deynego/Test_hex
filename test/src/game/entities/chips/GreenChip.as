package game.entities.chips {
public class GreenChip extends AbstractChip {
    public function GreenChip(radius:uint) {
        super(ChipType.GREEN, radius, ChipType.GREEN_TYPE);
    }
}
}
