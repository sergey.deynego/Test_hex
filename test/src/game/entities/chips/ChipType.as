package game.entities.chips {
public class ChipType {

    public static const RED_TYPE:uint = 0;
    public static const GREEN_TYPE:uint = 1;
    public static const BLUE_TYPE:uint = 2;
    public static const ORANGE_TYPE:uint = 3;
    public static const YELLOW_TYPE:uint = 4;
    public static const GRAY_TYPE:uint = 5;

    public static const MIN_CHIP_TYPE:int = 0;
    public static const MAX_CHIP_TYPE:int = 5;

    public static const RED:uint = 0xFF0000;
    public static const GREEN:uint = 0x00FF00;
    public static const BLUE:uint = 0x0000FF;
    public static const ORANGE:uint = 0xFF9000;
    public static const YELLOW:uint = 0xFFFF00;
    public static const GRAY:uint = 0xA1A1A1;

}
}
