package game.entities.chips {
public class YellowChip extends AbstractChip {
    public function YellowChip(radius:uint) {
        super(ChipType.YELLOW, radius, ChipType.YELLOW_TYPE);
    }
}
}
