package game.entities.gamefield {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.MouseEvent;

import game.entities.chips.ChipsFactory;
import game.entities.ifaces.IChip;
import game.grid.HexagonalGrid;
import game.grid.ifaces.IGrid;

public class GameField extends Sprite {
    public function GameField(width:int, height:int, columns:int) {
        super();
        this._width = width;
        this._height = height;
        this._columns = columns;
        this.addEventListener(Event.ADDED_TO_STAGE, $init)
    }

    /**
     *
     */
    protected var $backLayer:BackLayer;
    /**
     * Контейнер в котором располагаются фишки на игровом поле
     */
    protected var $chipsLayer:ChipsLayer;
    /**
     * Контейнер в который помещаем фишки подлежащие удалению с игрового поля
     * с применением (напрмер ) каких либо визуальных эффектов, движения и т.д.
     */
    protected var $dropedLayer:DropedChipsLayer;
    /**
     * Объет хранит параметры сетки тайлов (гексы, квадраты, треугольники или ромбы...
     */
    protected var $grid:IGrid;
    protected var $chipsFactory:ChipsFactory;
    private var _width:int;
    private var _height:int;
    private var _columns:int;

    protected function $initListeners(o:IEventDispatcher):void {
        if (!o.hasEventListener(MouseEvent.CLICK)) {
            o.addEventListener(MouseEvent.CLICK, this._onClick);
        }
        else {
            o.removeEventListener(MouseEvent.CLICK, this._onClick)
        }
    }

    /**
     * заполняем игровое поле фишками
     * @param density - плотность заполнения
     */
    protected function $fillField(density:Number = .70):void {
        var r:int = int(this.$grid.rows * density);
        var chip:IChip;
        for (var row:int = 0; row < r; row++) {
            for (var col:int = 0; col < this.$grid.columns; col++) {
                this.$chipsLayer.addChip(this.$chipsFactory.getRandomChips(), col, row);
            }
        }
    }

    private function testChip(chip:IChip):Boolean {
        var a:Array = this.$chipsLayer.selectChips(chip);
        if (a.length > 2) {
            this.$chipsLayer.removeChips(a);
            this.$chipsLayer.dropUnLinkedChips();
        }
        return a;
    }

    protected function $init(e:Event):void {
        e.stopImmediatePropagation();
        this.removeEventListener(Event.ADDED_TO_STAGE, $init);
        this.$grid = new HexagonalGrid(this._columns, this._width, this._height) as IGrid;
        this.$backLayer = this.addChild(new BackLayer() as DisplayObject) as BackLayer;
        this.$chipsLayer = this.addChild(new ChipsLayer(this.$grid) as DisplayObject) as ChipsLayer;
        this.$dropedLayer = this.addChild(new DropedChipsLayer() as DisplayObject) as DropedChipsLayer;
        this.$chipsFactory = new ChipsFactory((Math.min(this.$grid.cellWidth, this.$grid.cellHeight) >> 1) - 2);
        this.$initListeners(this.$chipsLayer as IEventDispatcher);
        this.$fillField();
    }

    /**
     * Обработка клика по фишке
     * @param e
     */
    private function _onClick(e:MouseEvent):void {
        trace(e.target)
        this.$chipsLayer.dropVisited();
        if (this.testChip(e.target as IChip)) {
            this.$chipsLayer.dropUnLinkedChips()
        }
    }


}
}
