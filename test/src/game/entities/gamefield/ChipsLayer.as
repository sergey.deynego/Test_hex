package game.entities.gamefield {
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.geom.Point;

import game.entities.ifaces.IChip;
import game.grid.hex.OffsetCoord;
import game.grid.ifaces.IGrid;

public class ChipsLayer extends Sprite {

    public function ChipsLayer(grid:IGrid) {
        this.$grid = grid;
    }

    private var $grid:IGrid;

    /**
     *  Добавим фишку в заданныую позицию.
     * @param chip
     * @param col
     * @param row
     */
    public function addChip(chip:IChip, col:int, row:int):void {
        var pt:Point = this.$grid.grid2pixel(new OffsetCoord(col, row));
        var o:DisplayObject = this.addChild(chip as DisplayObject);
        chip.column = col;
        chip.row = row;
        o.x = pt.x;
        o.y = pt.y;
        o.name = col + "_" + row;
    }

    /**
     *
     * @param col
     * @param row
     * @return
     */
    public function getChip(col:int, row:int):IChip {
        var chip:IChip = this.getChildByName(col + "_" + row) as IChip;
        return chip;
    }

    /**
     * Удаляем фишку из заданной позиции
     * @param col
     * @param row
     * @return
     */
    public function removeChip(col:int, row:int):IChip {
        try {
            return this.removeChild(this.getChildByName(col + "_" + row)) as IChip;
        } catch (e) {

        }
        return null;

    }

    /**
     * удалить с поля фишки находящиеся в списке
     * @param list
     */
    public function removeChips(list:Array):void {
        for each(var chip:DisplayObject in list) {
            try {
                chip.parent.removeChild(chip);
            } catch (e) {
                continue;
            }

        }
    }

    /**
     * Сбросит флаг посещенной фишки
     */
    public function dropVisited():void {
        var i:int = 0;
        var chipPtr:IChip;
        try {
            do {
                chipPtr = this.getChildAt(i) as IChip;
                chipPtr.notVisited();
                i++;
            } while (true)
        } catch (e) {

        }
    }

    /**
     * выбираем соседние фишки с типом как у заданной фишки
     * @param chip
     * @param count
     * @return
     */
    public function selectChips(chip:IChip):Array {
        if (chip.isVisited) return [];
        var a:Array = this.$grid.getNeigbor(chip.column, chip.row);
        var res:Array = new Array();
        var sChip:IChip;
        chip.visited();
        for each (var c:OffsetCoord in a) {
            sChip = this.getChip(c.col, c.row);
            if (!sChip) continue;
            if (sChip.isVisited) continue;
            if (sChip.type == chip.type) {
                res.push(sChip);
            }
        }
        a = [chip];
        for each(var ch:IChip in res) {
            a = a.concat(this.selectChips(ch))
        }
        return a;
    }

    /**
     * Удаляем фишки, которые не связаны с фишками первой строки
     * @param seed
     */
    public function dropUnLinkedChips():void {
        this.dropVisited();
        var chips:Array = new Array();
        var chip:IChip;
        // создаем массив начальных фишек от которых будем
        // проверять связанность
        for (var col:int = 0; col < this.$grid.columns; col++) {
            chip = this.getChip(col, 0);
            if (chip) chips.push(chip);
        }
        while (true) {
            try {
                this.markLinkedChips(chips.pop());
            } catch (e) {
                break;
            }
        }
        ;

        var i:int = 0;
        while (true) {
            try {
                chip = this.getChildAt(i) as IChip;
            } catch (e) {
                break;
            }

            if (!chip.isVisited) {
                (chip as DisplayObject).parent.removeChild(chip as DisplayObject);
                continue;
            }
            i++;
        }
        this.dropVisited();
    }
    /**
    * Отмкчаем фишки которые связаны с  заданной фишко
    */
    private function markLinkedChips(chip:IChip):Array {
        if (chip.isVisited) return [];
        var a:Array = this.$grid.getNeigbor(chip.column, chip.row);
        var res:Array = new Array();
        var sChip:IChip;
        chip.visited();
        for each (var c:OffsetCoord in a) {
            sChip = this.getChip(c.col, c.row);
            if (sChip) {
                if (!sChip.isVisited) {
                    res.push(sChip);
                }
            }
        }
        a = new Array();
        for each(var ch:IChip in res) {
            a = a.concat(this.markLinkedChips(ch))
        }
        return a;
    }
}
}
