package game.entities.ifaces {
public interface IChip {

    function get name():String;

    function get type():uint;

    function get column():int;

    function set column(c:int):void;

    function get row():int;

    function set row(r:int):void;

    function get isVisited():Boolean;

    function visited():void;

    function notVisited():void;

}
}
