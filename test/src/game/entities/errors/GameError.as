package game.entities.errors {
public class GameError extends Error {

    public static const UNKNOWN_CHIP_ID:String = "Unknown chip id";
    public static const ILLEGAL_HEX_PARAMS:String = "Illegak Hexagon params"

    public static const UNKNOWN_CHIP_ID_CODE:uint = 0xF00001;
    public static const ILLEGAL_HEX_PARAMS_CODE:uint = 0xF00011;

    public function GameError(message:* = "", id:* = 0) {
        super(message, id);
    }
}
}
