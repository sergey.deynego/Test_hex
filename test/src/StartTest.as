package {
import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;

import game.entities.gamefield.GameField;

[SWF(height="700", width="800", frameRate="24", backgroundColor=0xFFFFFF)]
public class StartTest extends Sprite {

    private static const WIDTH:int = 800;
    private static const HEIGHT:int = 700;
    private static const COLUMNS:int = 40;

    public function StartTest() {
        super();
        this.stage.scaleMode = StageScaleMode.NO_SCALE;
        this.stage.align = StageAlign.TOP_LEFT;
        this.addEventListener(Event.ADDED_TO_STAGE, this.$start);
    }

    private var gameField:GameField;

    protected function $start(e:Event):void {
        e.stopImmediatePropagation();
        this.removeEventListener(Event.ADDED_TO_STAGE, this.$start);
        this.gameField = this.addChild(new GameField(WIDTH, HEIGHT, COLUMNS)) as GameField
    }
}
}
